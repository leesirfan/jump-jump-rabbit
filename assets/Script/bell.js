// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        speedY: -1,

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },

    update(dt) {
        this.node.y = this.node.y + this.speedY;
        if (this.node.y <= 350) {
            if (this.node.opacity > 0) {
                this.node.opacity = this.node.opacity - 10;
            } else {
                //生成新的铃铛
                cc.find("Canvas/Main Camera").getComponent("main").buildBell();
                //移除这个铃铛
                this.node.removeFromParent();
            }
        }

        if (this.node.y < cc.find("rabbit").y) {
            if (cc.find("rabbit").y - this.node.y > 540) {
                //生成新的铃铛
                cc.find("Canvas/Main Camera").getComponent("main").buildBell();
                //移除这个铃铛
                this.node.removeFromParent();
            }
        }
    },





});