// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        shalou: 0,
        shalouMax: 2,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {},

    start() {

    },
    end() {
        this.node.removeComponent(cc.Sprite);
    },
    update(dt) {
        if (this.shalou < this.shalouMax) {
            this.shalou = this.shalou + dt
        } else {
            this.shalou = 0;
            this.node.removeFromParent();
        }
    },
});