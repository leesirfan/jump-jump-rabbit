// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        speedX: 2,
        minX: 20,
        maxX: 580,
        state: 0, //0  正常飞行  1 被踩了
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },

    update(dt) {
        if (this.state == 0) {
            if (this.node.x > this.maxX) {
                this.node.scaleX = -1;
                this.speedX = -(Math.abs(this.speedX));

            } else if (this.node.x < this.minX) {
                this.node.scaleX = 1;
                this.speedX = (Math.abs(this.speedX));
            }
            this.node.x = this.node.x + this.speedX;
        }

        if (this.node.y < cc.find("rabbit").y) {
            if (cc.find("rabbit").y - this.node.y > 540) {
                //移除这个铃铛
                this.node.removeFromParent();
            }
        }
    },
    //被踩到，飞走
    run() {
        this.node.getComponent(cc.AudioSource).play();
        //
        this.node.removeComponent(cc.BoxCollider);
        let move1 = cc.moveTo(0.1, cc.v2(this.node.x, this.node.y - 100));
        let move2 = null;
        if (this.node.scaleX == 1) {
            move2 = cc.moveTo(2, cc.v2(this.node.x + 640, this.node.y - 100))
        } else {
            move2 = cc.moveTo(2, cc.v2(this.node.x - 640, this.node.y - 100))
        }
        move1.easing(cc.easeOut(1.5));
        move2.easing(cc.easeOut(1.5));
        let callback = cc.callFunc(function() {
            this.node.removeFromParent();
        }, this);
        this.node.runAction(cc.sequence(move1, move2, callback));
    }
});