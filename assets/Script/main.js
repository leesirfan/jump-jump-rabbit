// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        gameState: 1, //0 游戏结束 1 游戏进行中
        maxScore: 0, //最高分
        score: 0,
        bellCount: 0,
        lastBell: {
            type: cc.Node,
            default: null
        },
        lastBellY: 640,
        birdRate: 0.2, //生成鸟的概率
        //rabbitState: 0, //0站立idle   1跑动run    2跳跃jump  3 
        rabbit: {
            type: cc.Node,
            default: null
        },
        jumpYspeed: 120, //跳跃的y速度
        touchX: 320, //触摸的x坐标
        speedX: 10, //向鼠标水平移动的速度
        speedY: 0, //自然下落的速度
        maxSpeedY: -20, //最大下落速度
        addSpeedY: -1, //自然下落加速度
        rabbitAnimation: {
            type: cc.Animation,
            default: null
        },
        //分数面板，在掉落的时候显示
        scorePanel: {
            type: cc.Node,
            default: null
        },
        //各类预制体
        prefabBell5: {
            type: cc.Prefab,
            default: null
        },
        prefabFallBell5: {
            type: cc.Prefab,
            default: null
        },
        prefabBell4: {
            type: cc.Prefab,
            default: null
        },
        prefabFallBell4: {
            type: cc.Prefab,
            default: null
        },
        prefabBell3: {
            type: cc.Prefab,
            default: null
        },
        prefabFallBell3: {
            type: cc.Prefab,
            default: null
        },
        prefabBell2: {
            type: cc.Prefab,
            default: null
        },
        prefabFallBell2: {
            type: cc.Prefab,
            default: null
        },
        prefabBell1: {
            type: cc.Prefab,
            default: null
        },
        prefabFallBell1: {
            type: cc.Prefab,
            default: null
        },
        prefabSnow: {
            type: cc.Prefab,
            default: null
        },
        prefabStar: {
            type: cc.Prefab,
            default: null
        },
        prefabBird: {
            type: cc.Prefab,
            default: null
        },
        prefabScoreText: {
            type: cc.Prefab,
            default: null
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {


        //获取存储的最高分
        let maxScore = cc.sys.localStorage.getItem('maxScore');

        if (maxScore == null || maxScore == "") {

        } else {
            this.maxScore = parseInt(maxScore);
        }
        cc.find("ScorePanel/Score2").getComponent(cc.Label).string = this.maxScore;
        //开启碰撞
        cc.director.getCollisionManager().enabled = true;
        for (let i = 0; i < 12; i++) {
            this.buildBell();
        }
        this.node.on(cc.Node.EventType.TOUCH_START, this.touchFun, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.moveFun, this);
        this.rabbitAnimation.play("idle");
    },
    onDestroy() {
        this.node.off(cc.Node.EventType.TOUCH_START, this.touchFun, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.moveFun, this);

    },
    start() {

    },
    update(dt) {
        //#region   处理y坐标
        if (this.rabbit.y <= 240) {
            this.rabbit.y = 240;
            this.speedY = 0;

            if (this.rabbitAnimation.currentClip.name == "idle" || this.rabbitAnimation.currentClip.name == "stop" || this.rabbitAnimation.currentClip.name == "run") {

            } else {
                this.rabbitAnimation.play("stop");
            }
            //地面检查是否结束
            if (this.gameState == 1) {
                if (this.score > 0) {
                    this.gameState = 0;
                    //分数登记
                    if (this.score > this.maxScore) {
                        cc.find("ScorePanel/Label1").getComponent(cc.Label).string = "新纪录";
                        //记录最高分
                        cc.sys.localStorage.setItem('maxScore', this.score.toString());
                    } else {
                        cc.find("ScorePanel/Label1").getComponent(cc.Label).string = "你的分数";
                    }
                    cc.find("ScorePanel/Score1").getComponent(cc.Label).string = this.score;
                    this.scorePanel.active = true;
                }
            }

        } else {
            //计算y轴速度
            if (this.speedY < this.maxSpeedY) {
                this.speedY = this.maxSpeedY;
            } else {
                this.speedY = this.speedY + this.addSpeedY;
            }
            //进行下落
            this.rabbit.y = this.rabbit.y + this.speedY;
        }
        //#endregion

        //#region  处理x 坐标
        if (Math.abs(this.rabbit.x - this.touchX) > this.speedX) {
            if (this.rabbit.x > this.touchX) {
                this.rabbit.x = this.rabbit.x - this.speedX;
                this.rabbit.scaleX = -1;
            } else {
                this.rabbit.x = this.rabbit.x + this.speedX;
                this.rabbit.scaleX = 1;
            }

            if (this.rabbit.y <= 240) {
                //在地面需要跑动
                if (this.rabbitAnimation.currentClip.name != "run") {
                    this.rabbitAnimation.play("run");
                }
            }


        } else {

            if (this.rabbit.y <= 240) {
                //在地面需要静止
                if (this.rabbitAnimation.currentClip.name == "idle" || this.rabbitAnimation.currentClip.name == "stop") {

                } else {
                    this.rabbitAnimation.play("stop");
                }
            }
        }
        //#endregion



        //#region  摄像机移动

        if (this.rabbit.y < 540) {
            this.node.y = 0;
        } else {
            this.node.y = this.rabbit.y - 540;
        }

        //#endregion



    },
    touchFun(event) {
        let herePos = event.getLocation();
        this.touchX = herePos.x;
        if (this.score == 0 && this.speedY == 0) {
            this.rabbit.y = 245;
            this.speedY = this.jumpYspeed;
        }
        this.rabbit.getComponent(cc.Animation).play("jump");


    },
    moveFun(event) {

        let herePos = event.getLocation();
        this.touchX = herePos.x;

    },
    jump() {
        this.speedY = this.jumpYspeed;
        this.rabbit.getComponent(cc.Animation).play("jump");
    },
    buildBell() {

        //生成铃铛   根据已经踩过的铃铛数量，决定要生成多大的铃铛
        // 0-20  bell 5
        // 21-50 bell 4
        // 51-100 bell 3
        // 101-180 bell 2
        // >180 bell 1
        let bell = null;
        if (this.bellCount > 180) {
            bell = cc.instantiate(this.prefabBell1);
        } else if (this.bellCount > 100) {
            bell = cc.instantiate(this.prefabBell2);
        } else if (this.bellCount > 50) {
            bell = cc.instantiate(this.prefabBell3);
        } else if (this.bellCount > 20) {
            bell = cc.instantiate(this.prefabBell4);
        } else {
            bell = cc.instantiate(this.prefabBell5);
        }

        bell.x = 20 + Math.random() * 560;

        if (this.lastBell != null) {
            this.lastBellY = this.lastBell.y;
        }
        bell.y = this.lastBellY + 60 + Math.random() * 60;
        this.lastBellY = bell.y;
        this.lastBell = bell;
        cc.director.getScene().addChild(bell);

        //生成3-5片雪花

        let count = 1 + Math.ceil(Math.random() * 2);
        for (let i = 0; i < count; i++) {
            let snow = cc.instantiate(this.prefabSnow);
            snow.x = Math.random() * 640;
            snow.y = bell.y + Math.random() * 80;
            cc.director.getScene().addChild(snow);
        }

        //每踩10个铃铛，进行一次随机生成鸟
        if (this.bellCount > 0 && (this.bellCount % 10 == 0)) {
            if (Math.random() < this.birdRate) {
                let bird = cc.instantiate(this.prefabBird);
                bird.x = 20 + Math.random() * 560;
                bird.y = this.lastBellY + 80 + Math.random() * 10;
                this.lastBellY = bird.y;
                this.lastBell = bird;
                cc.director.getScene().addChild(bird);
            }
        }

    },

    //
    bellFall(x, y, bellname) {
        let fallBell = null;
        switch (bellname) {
            case "bell5":
                fallBell = cc.instantiate(this.prefabFallBell5);
                break;
            case "bell4":
                fallBell = cc.instantiate(this.prefabFallBell4);
                break;
            case "bell3":
                fallBell = cc.instantiate(this.prefabFallBell3);
                break;
            case "bell2":
                fallBell = cc.instantiate(this.prefabFallBell2);
                break;
            case "bell1":
                fallBell = cc.instantiate(this.prefabFallBell1);
                break;

        }
        fallBell.x = x;
        fallBell.y = y;
        cc.director.getScene().addChild(fallBell);
        let len = 100;
        //炸裂开来
        for (let i = 0; i < 15; i++) {
            let star = cc.instantiate(this.prefabStar);
            star.x = x;
            star.y = y;
            let targetX = x + Math.sin(Math.PI / 180 * (i * 24)) * len;
            let targetY = y + Math.cos(Math.PI / 180 * (i * 24)) * len;
            let move = cc.moveTo(0.8, cc.v2(targetX, targetY));
            move.easing(cc.easeOut(1.5));
            let callback = cc.callFunc(function() {
                star.removeFromParent();
            }, star);
            cc.director.getScene().addChild(star);
            star.runAction(cc.sequence(move, callback));
        }
    },
    bellScore(x, y) {
        this.bellCount++;
        this.score = this.score + this.bellCount * 10;
        let scoreText = cc.instantiate(this.prefabScoreText);
        scoreText.getComponent(cc.Label).string = (this.bellCount * 10).toString();
        scoreText.x = x;
        scoreText.y = y;
        cc.find("Canvas/Main Camera/Score").getComponent(cc.Label).string = this.score;
        cc.director.getScene().addChild(scoreText);

    },
    birdScore(x, y) {
        let scoreText = cc.instantiate(this.prefabScoreText);
        this.score = this.score * 2;
        scoreText.getComponent(cc.Label).string = "奖励翻倍";
        scoreText.x = x;
        scoreText.y = y;
        cc.find("Canvas/Main Camera/Score").getComponent(cc.Label).string = this.score;
        cc.director.getScene().addChild(scoreText);
    }

});