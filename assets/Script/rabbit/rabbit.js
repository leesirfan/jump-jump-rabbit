// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,
    properties: {
        de: {
            type: cc.Node,
            default: null,
        },

        speedX: 0, //
        speedY: 0, //自然下落的速度
        maxSpeedY: 30, //最大y速度
        addSpeedY: 4, //自然下落加速度
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad() {
    // },
    // start() {

    // },
    // update(dt) {
    // }


    //非物理碰撞检测
    onCollisionEnter(other, self) {

        //跳跃
        cc.find("Canvas/Main Camera").getComponent("main").jump();

        //踩到铃铛
        if (other.node.group == "bell") {

            cc.find("Canvas/Main Camera").getComponent("main").buildBell();
            cc.find("Canvas/Main Camera").getComponent("main").bellFall(other.node.x, other.node.y, other.node.name);
            cc.find("Canvas/Main Camera").getComponent("main").bellScore(other.node.x, other.node.y); //显示分数 计分  
            other.node.removeFromParent();
        }
        //踩到鸟
        if (other.node.group == "bird") {
            other.node.getComponent("bird").run();
            cc.find("Canvas/Main Camera").getComponent("main").birdScore(other.node.x, other.node.y); //显示翻倍  分数翻倍

        }

    },


    jumpEnd() {
        this.node.getComponent(cc.Animation).play("fall");
    },
    stopEnd() {
        this.node.getComponent(cc.Animation).play("idle");
    }




});