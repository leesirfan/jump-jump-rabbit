// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        speedY: -1,
        speedX: 0.5,
        shalou: 0,
        shalouMax: 1,

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.speedY = this.speedY - Math.random();
        let seed = Math.random();
        let len = seed * 20;
        this.node.width = len;
        this.node.height = len;
        this.speedX = this.speedX * seed;
        this.shalouMax = this.shalouMax * seed;

    },

    start() {

    },

    update(dt) {
        this.node.y = this.node.y + this.speedY;

        if (this.shalou <= this.shalouMax) {
            this.shalou = this.shalou + dt;
            this.node.x = this.node.x + this.speedX;
        } else {
            this.shalou = 0;
            this.speedX = -this.speedX;
        }



        if (this.node.y <= 0) {
            this.node.removeFromParent();
        }

        if (this.node.y < cc.find("rabbit").y) {
            if (cc.find("rabbit").y - this.node.y > 540) {
                //移除这个
                this.node.removeFromParent();
            }
        }
    },
});