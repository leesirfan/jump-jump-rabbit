// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.startGame, this);
    },
    onDestroy() {
        this.node.off(cc.Node.EventType.TOUCH_START, this.startGame, this);
    },
    start() {

    },
    startGame(event) {
        cc.director.loadScene("main");


        // cc.director.preloadScene("main", function(count, amount, item) {

        //     let len = (count / amount) * 565;
        //     if (len >= cc.find("bar/barin").width) {
        //         cc.find("bar/barin").width = len;
        //     }

        // }.bind(this), function() {

        //     cc.find("bar").active = false;
        //     cc.find("barLabel").active = false;
        //     cc.find("button").active = true;
        //     cc.director.loadScene("Game");
        // }.bind(this));

    }

    // update (dt) {},
});