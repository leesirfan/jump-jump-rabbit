// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        prefabStar: {
            type: cc.Prefab,
            default: null
        }
    },
    // LIFE-CYCLE CALLBACKS:
    onLoad() {



        this.node.on(cc.Node.EventType.MOUSE_DOWN, this.clickFunction, this);
    },
    onDestroy() {
        this.node.off(cc.Node.EventType.MOUSE_DOWN, this.clickFunction, this);
    },
    start() {

    },
    clickFunction(event) {
        let oriX = 450;
        let oriY = 450;
        let len = 100;

        for (let i = 0; i < 15; i++) {
            let star = cc.instantiate(this.prefabStar);
            star.x = oriX;
            star.y = oriY;
            let targetX = oriX + Math.sin(Math.PI / 180 * (i * 24)) * len;
            let targetY = oriX + Math.cos(Math.PI / 180 * (i * 24)) * len;
            let move = cc.moveTo(0.8, cc.v2(targetX, targetY));
            move.easing(cc.easeOut(1.5));
            let callback = cc.callFunc(function() {
                star.removeFromParent();
            }, star);
            cc.director.getScene().addChild(star);
            star.runAction(cc.sequence(move, callback));
        }

        // let oriX = 450;
        // let oriY = 450;
        // let len = 50;

        // for (let i = 0; i < 15; i++) {
        //     let star = cc.instantiate(this.prefabStar);
        //     star.x = oriX + Math.sin(Math.PI / 180 * (i * 24)) * len;
        //     star.y = oriX + Math.cos(Math.PI / 180 * (i * 24)) * len;
        //     cc.director.getScene().addChild(star);
        // }



    }

    // update (dt) {},
});